import React, { useState, Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

export const Header = () => {
	const userData = useSelector(userData => userData);
	
	const [isActive, settoggleClass] = useState(false);
	
	
	
	const rctToggle = (event) => {
		settoggleClass(!isActive);

 		}

	return (
		<Fragment>

			<div className="fix_header wow fadeInDown animated" >
				<div id="header">
					<div className="doc_width">
						<div className="logo"><NavLink to="/" > <img src="/images/logo.png" alt="" border="0" /> </NavLink>
						</div>

						<div className="header_right">
							<div className="login_menu">

								<li className="myaccount1" style={{ position: 'relative' }}>
									<NavLink className="tringle_img plus_icon" onClick={rctToggle} to="/home">
										All Auctions <img src="/images/tringle_icon.png" alt="" border="0" />  </NavLink>
										
									<div className={isActive ? 'menu-show slide_mayaccount slide_mayaccount_auction': 'slide_mayaccount slide_mayaccount_auction'} >
										<a href="auctions.html">Live Auctions</a>
										<a href="future.html">Upcoming Auctions</a>
										<a href="closed.html">Winners Auctions</a>
									</div>
								</li>

								<li><NavLink to="/packages">Buy Bids</NavLink></li>
								<li><NavLink to="/wheel">Spin Wheel</NavLink></li>

								<li className="menu_last myaccount1" style={{ position: 'relative' }}>
									<NavLink className="tringle_img plus_icon_how" to="/how_it_works">
										How Propenny Works <img src="/images/tringle_icon.png" alt="" border="0" />  </NavLink>
									<div className="slide_mayaccount slide_mayaccount_how" style={{ display: 'none' }}>
										<a href="penny_auctions_guide.html">Penny Auctions Guide</a>
										<a href="seat_auctions_guide.html">Seat Auctions Guide</a>
										<a href="unique_auctions_guide.html">Unique Auction Guide</a>
										<a href="ebay_auctions_guide.html">Ebay Auction Guide</a>
									</div>
								</li>
													
										{
										userData.login.user_id > 0 ?
											<Fragment>
												<NavLink className="btn_new left-animated right-animated btn-blue" to="/logout">Logout</NavLink>
												<li className="welcomeuser"><NavLink  to="/users"> <font>Hi,</font> {userData.login.username} </NavLink></li>
											</Fragment>
											:
											<Fragment>
												<NavLink to="/login">Login</NavLink>
												<NavLink className="btn_new   left-animated right-animated btn-blue" to="/signup">Register</NavLink>
											</Fragment>
								    	}
								
							
							</div>
						</div>
					</div>
				</div>




			<div class="main_menu mobile_none">
			<div class="doc_width">
			<a onclick="document.getElementById('navigation').classList.toggle('closed');" class="nav-btn" href="javascript:void(0)">&nbsp;</a>
			 <ul id="#navigation" className="slide_login closed1">
	
				{
					userData.login.user_id > 0 ?
					<Fragment>
					<li className="tasktop_non welcome_name"><NavLink  to="/users"> Hi, {userData.login.username} </NavLink></li>
					<li><NavLink  to="/logout">Logout</NavLink></li>				
					</Fragment>
						:
					<Fragment>
					<NavLink to="/login">Login</NavLink>
					<NavLink className="btn_new   left-animated right-animated btn-blue" to="/signup">Register</NavLink>
					</Fragment>
				}
		
			<li className="dropdown" style={{ position: 'relative' }}>
				<NavLink to="/home"> All Auctions <img className="dropdown-tringle" src="/images/tringle_icon.png" alt="" border="0" />  </NavLink>
					
				<div className="dropdown-menu">
					<a href="auctions.html">Live Auctions</a>
					<a href="future.html">Upcoming Auctions</a>
					<a href="closed.html">Winners Auctions</a>
				</div>
			</li>
			<li><NavLink to="/packages">Buy Bids</NavLink></li>
			<li><NavLink to="/wheel">Spin Wheel</NavLink></li>
			<li className="dropdown menu_last" style={{ position: 'relative' }}>
				<NavLink to="/how_it_works">How Propenny Works <img className="dropdown-tringle" src="/images/tringle_icon.png" alt="" border="0" />  </NavLink>
				<div className="dropdown-menu">
					<a href="penny_auctions_guide.html">Penny Auctions Guide</a>
					<a href="seat_auctions_guide.html">Seat Auctions Guide</a>
					<a href="unique_auctions_guide.html">Unique Auction Guide</a>
					<a href="ebay_auctions_guide.html">Ebay Auction Guide</a>
				</div>
			</li>
			</ul>
			</div>
			</div>




			</div>
			<div id="flash_msg" className="flash_msg main_error" style={{ display: 'none' }} >
				<div id="error_inner"  className="error_inner"></div>
			</div>
		</Fragment>

	)
}


