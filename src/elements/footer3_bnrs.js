import React from 'react';

function Footer3Bnr(props) {
    return (
      
<div className="btm_bnr">
  <div className="doc_width">

<div className="block_box width_33">
<div className="box_content">
<a href="">
<div className="content-overlay content-overlay-blue"></div>
<img className="content-image" src="images/support_img.jpg" />
<div className="content-details fadeIn-top">
<h3>Support 24/7</h3>
<p> Contact us in any moment, you can create a support ticket or simply talk with us by the live chat. If live chat it is offline, create a support ticket or send us a email.</p>
</div>
</a>
</div>
</div>

<div className="block_box width_33">
<div className="box_content">
<a href="">
<div className="content-overlay content-overlay-green"></div>
<img className="content-image" src="images/happy_img.jpg" />
<div className="content-details fadeIn-top">
<h3>Change your won item for Cash </h3>
<p> On propennyauction you can choose, want the product auction that you won or prefer the product 
  value in cash? ... You dedice what you want after pay the won auction.</p>
</div>
</a>
</div>
</div>

<div className="block_box width_33">
<div className="box_content">
<a href="">
<div className="content-overlay content-overlay-purpule"></div>
<img className="content-image" src="images/ticke_support_img.jpg" />
<div className="content-details fadeIn-top">
<h3>Multiple auctions types</h3>
<p> Into the Auctions world propennyauction everything its possible, we have a lot of diferent types of auctions, and we work every day to give your more and more auctions types. </p>
</div>
</a>
</div>
</div>
</div>
</div>
    );
}

export default Footer3Bnr;