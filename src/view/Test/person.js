import React, { useState } from 'react'

function Persona(){ 
 const [numbers, setNumber] = useState([])

 const addNumbers = () => { 
     console.log('in function: ' + Math.floor(( Math.random * 10))  )
    setNumber([
        ...numbers, {
            id: numbers.length,
            val: Math.floor(Math.random() * 10) + 1
        }
    ]

    )
 }

return (
    <div> 
     <h2> <button onClick={addNumbers}>add number</button>  </h2> 
     <ul> <li> aa</li><li> aa</li> 
        {
            numbers.map(num =>( 
                
                <li key={num.id} >{num.val}</li>
            ))
        }

     </ul>
     <p>{JSON.stringify(numbers)}</p>
                                       
    </div>
)

}

export default Persona