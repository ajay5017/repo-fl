import React, { Component } from 'react'
 
 class Test extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cnt: 0,
      name: 'Welcome to you'
    }
    this.clickMe = this.clickMe.bind(this)    // binding this in constructor 
  }

  change5(){
    this.change()
    this.change()
    this.change()
    this.change()
    this.change()
  }
  change(){
    // this.setState
    //this.state.cnt = this.state.cnt + 1 
    // this.setState({
    //   cnt: this.state.cnt + 1 
    // }, ()=>{
    //   console.log(this.state.cnt )
    // }) 
    
    this.setState( prevState => ({
        cnt: prevState.cnt + 1 
    }), ()=>{
      console.log(this.state.cnt )
    })
     
   }
   clickMe(){
     this.setState( prevState => ({
       name: prevState.name + ' Googd bye '  
     }))
   }

  render() {
    if( this.state.cnt == 1 ){
      return <div>hello </div>
    
  }else{
    return <div>hi </div>
  }

    return (
      <div> <br /><br /><br /><br />
        <h1>Hello, world!</h1>
        <h2>Count -  {this.state.cnt } </h2>
        <button onClick={() => this.change5()}> Increment </button>

        <p>Hello: {this.state.name} </p>
        <button onClick={this.clickMe}> Increment </button>
        

      </div>
    ) 
      
  }
}
 
export default Test