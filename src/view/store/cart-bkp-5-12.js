import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { useSelector } from 'react-redux';
import { PayPalButton } from "react-paypal-button-v2";

import Checkout from '../../paypal/checkout';
import { functions } from '../../functions';

// import "./cart.scss"
import './style.css'


const Cart = (props) => {
  const [shipping, setShipping] = useState(0)
  const [total, setTotal] = useState(0)
  const [products, setProducts] = useState([])

  const userData = useSelector(userData => userData);

  useEffect(() => {
    const getProducts = async () => {
      //console.log(`page loading : `);
      let req = await axios.get(`http://dev.propennyauction.com/api/cart_products.php?user_id=${userData.login.user_id}`);
      let res = await req;
      console.log(res);
      setProducts(res.data.data)
      let subTotal = res.data.data.reduce((total, amount) => total + parseInt(amount.price, 10), 0);
      setTotal(subTotal)

      //props.add_products(res.data.data);
    }
    getProducts()
    console.log(products)

  }, []);

  const remove_from_cart = (cartItem, index) => {
    //await props.remove_from_cart(cartItem.id);
    //await props.add_cart_length(props.cart.length)

    let duplicateProducts = [...products];
    duplicateProducts.splice(index, 1);
    setProducts(duplicateProducts);
    functions.flash_msg('Cart updated Successfully');


    let req = axios.post("http://dev.propennyauction.com/api/cart.php?action=delete", {
      cart_id: cartItem.id
    });
    req.then(res => {
      console.log(res.data.data);

    }).catch(err => {
      console.log('err:' + err);
    })
    //window.location.reload();
  }






  return (
    
    
    <div className="doc_width ">
    <div class="step_titel mar_b_0"><h1>Store </h1></div>

    <div className="cart_left shadow_bg f_l">
      <div className="cart_header">
        <div className="item_details">Item(s) Details</div> 
        <div className="quantity">Quantity</div>
        <div className="amount">Amount</div>
      </div>

    <div className="item">
      <div className="pro_box">
        <div className="pro_img">
          <img src="/images/logo.png" alt="" border="0" />
        </div>

        <div className="cart-item-details">
          <div className="title">Morpankhi Pure Silk Maheshwari Saree</div>
          <div className="price">$150</div>
          <div className="remove">X</div>
        </div>
      </div>

      <div className="quantity-box">
          <input type="text" class="qty" value="1" />
      </div>

      <div className="price-right text-right">
        $7,900
      </div>      

    </div>
    </div>

    <div className="cart_right shadow_bg f_r">
      right cart
    </div>

<br></br>
    <div className="cart_left shadow_bg f_l">
    <header className="cart-header">
        <strong>Items in Your Cart</strong>        
      </header>
    <div className="cart_header">
        <div className="item_details">Item(s) Details</div> 
        <div className="quantity">Quantity</div>
        <div className="amount">Amount</div>
      </div>



      <div class="cart-table">

        <ul>

          {

            products.map((cartItem, index) => {

              return (
                <li class="item" key={index}>
                  <div class="item-main">
                    <div class="item-block ib-info">
                      <img class="product-img" src={cartItem.img} />
                      <div class="ib-info-meta">
                        <span class="title">{cartItem.title}</span>
                        <span class="itemno">{cartItem.category_name}</span>
                      </div>
                    </div>
                    <div class="item-block ib-qty">
                      <input type="text" value={cartItem.quantity} class="qty" />
                      <span class="price"><span>x</span> ${cartItem.price}</span>
                    </div>
                    <div class="item-block ib-total-price">
                      <span class="tp-price">${cartItem.quantity * cartItem.price}</span>
                      <span class="tp-remove"><i class="i-cancel-circle"></i></span>
                    </div>
                  </div>
                  <div class="item-foot cf">
                    <div class="if-message"><p>Space Reserved for item/promo related messaging</p></div>
                    <div class="if-left"><span class="if-status">In Stock</span></div>
                    <div class="if-right"><span class="blue-link" onClick={() => remove_from_cart(cartItem, index)}>Remove</span></div>
                  </div>
                </li>
              )
            })
          }
          {/* <!-- end simple product --> */}







          {/* <!-- end bundle product --> */}
        </ul>
      </div>
      <div class="sub-table cf">
        <div class="summary-block">

          <ul>
            <li class="subtotal"><span class="sb-label">Subtotal</span><span class="sb-value">${total}</span></li>
            <li class="shipping"><span class="sb-label">Shipping</span><span class="sb-value">${shipping}</span></li>
            {/* <li class="tax"><span class="sb-label">Est. Tax | <span class="tax-edit">edit <i class="i-notch-down"></i></span></span><span class="sb-value">$5.00</span></li> */}
            <li class="tax-calculate"><input type="text" value="06484" class="tax" /><span class="btn">Calculate</span></li>
            <li class="grand-total"><span class="sb-label">Total</span><span class="sb-value">${total + shipping}</span></li>
          </ul>
        </div>

      </div>

      <div class="cart-footer cf">
        {/* <span class="btn">Checkout</span> */}
        <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
          <Checkout total={props.cart_total} />
        </div>
        <span class="cont-shopping"><i class="i-angle-left"></i>Continue Shopping</span>
          <PayPalButton
          amount="0.01"
          // shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
          onSuccess={(details, data) => {
            alert("Transaction completed by " + details.payer.name.given_name);

            // OPTIONAL: Call your server to save the transaction
            return fetch("/paypal-transaction-complete", {
              method: "post",
              body: JSON.stringify({
                orderID: data.orderID
              })
            });
          }}
        />
      </div>
      </div>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    cart: state.cartReducer.cart,
    cart_total: state.cartReducer.cart_total,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    update_total: (total) => dispatch({ type: "UPDATE_TOTAL", payload: { total: total } }),
    remove_from_cart: (productId) =>
      dispatch({ type: "REMOVE_FROM_CART", payload: { productId: productId } }),
    add_cart_length: (cart_length) =>
      dispatch({ type: "ADD_CART_LENGTH", payload: { cart_length: cart_length } }),
  };
};
//export default connect(mapStateToProps, mapDispatchToProps)(Cart);
export default Cart;