import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
import Checkout from '../../paypal/checkout';
// import "./cart.scss"
const Cart = (props) => {
  const [shipping , setShipping] = useState(5)
  useEffect(() => {
    let amount = 0;
    props.cart.map(p => {
      amount = amount + parseFloat(p.buy_now);
    });
    props.update_total(amount)
  }, [])

 
    return (
        <div class="wrap">

        <header class="cart-header cf">
          <strong>Items in Your Cart</strong>
          {/* <span class="btn">Checkout</span> */}
        </header>
        {/* <div class="bonus-products">
          <strong>Bonus Products Block <span class="bp-toggle">(hide this block)</span></strong>
          
        </div> */}
        <div class="cart-table">
          <ul>
      
          {
            props.cart.map((cartItem) => {
              return (
                <li class="item">
              <div class="item-main cf">
                <div class="item-block ib-info cf">
                  <img class="product-img" src={cartItem.img} />
                  <div class="ib-info-meta">
                    <span class="title">{cartItem.title}</span>
                    <span class="itemno">{cartItem.category_name}</span>
                  </div>
                </div>
                <div class="item-block ib-qty">
                  <input type="text" value={cartItem.quantity} class="qty" />
                  <span class="price"><span>x</span> ${cartItem.buy_now}</span>
                </div>
                <div class="item-block ib-total-price">
                  <span class="tp-price">${cartItem.quantity*cartItem.buy_now}</span>
                  <span class="tp-remove"><i class="i-cancel-circle"></i></span>
                </div>         
              </div>
              <div class="item-foot cf">
                 <div class="if-message"><p>Space Reserved for item/promo related messaging</p></div> 
                <div class="if-left"><span class="if-status">In Stock</span></div>
                <div class="if-right"><span class="blue-link" onClick={async() => {

               await  props.remove_from_cart(cartItem.id);
               await  props.add_cart_length(props.cart.length)
               let req = axios.post("http://dev.propennyauction.com/api/cart.php?action=delete", {
                cart_id:cartItem.id
											});
											await req.then(res => {
												console.log(res);

											}).catch(err => {
												console.log('err:' + err);
											})
               window.location.reload();
                }}>Remove</span></div>
              </div>
            </li>
              )
            })
          }
      {/* <!-- end simple product --> */}
      
      
      {/* <!-- begin variation product w/ option --> */}
          {/* <li class="item">
              <div class="item-main cf">
                <div class="item-block ib-info cf">
                  <img class="product-img" src="http://fakeimg.pl/150/e5e5e5/adadad/?text=IMG" />
                  <div class="ib-info-meta">
                    <span class="title">Drink Up Nalgene Water Bottle</span>
                    <span class="itemno">#3498765</span>
                    <span class="styles">
                      <span><strong>Color</strong>: Neon Green</span>
                      <span><strong>Size</strong>: 32oz</span>
                      <span><strong>Warranty</strong>: 3 Years</span>
                      <span class="blue-link">Edit Details</span>
                    </span>
                  </div>  
                </div>
                <div class="item-block ib-qty">
                  <input type="text" value="3" class="qty" />
                  <span class="price"><span>x</span> $25.00</span>
                </div>
                <div class="item-block ib-total-price">
                  <span class="tp-price">$75.00</span>
                  <span class="tp-remove"><i class="i-cancel-circle"></i></span>
                </div>         
              </div>
              <div class="item-foot cf">
                <div class="if-message"><p> Item/promo related messaging shows up here</p></div>
                <div class="if-left"><span class="if-status">In Stock</span></div>
                <div class="if-right"><span class="blue-link">Gift Options</span> | <span class="blue-link">Add to Registry</span> | <span class="blue-link">Add to Wishlist</span></div>
              </div>
            </li> */}
      {/* <!-- end variation product w/ option --> */}
      
      
      
      {/* <!-- begin bundle product -->
            <li class="item">
              <div class="item-main cf">
                <div class="item-block ib-info cf">
                  <img class="product-img" src="http://fakeimg.pl/150/e5e5e5/adadad/?text=IMG" />
                  <div class="ib-info-meta">
                    <span class="title">Drink Up Nalgene Water Bottle</span>
                    <span class="itemno">#3498765</span>
                    <span class="styles">
                      <span><strong>Color</strong>: Neon Green</span>
                      <span><strong>Size</strong>: 32oz</span>
                      <span><strong>Warranty</strong>: 3 Years</span>
                      <span class="blue-link">Edit Details</span>
                    </span>
                  </div>
                </div>
                <div class="item-block ib-qty">
                  <input type="text" value="3" class="qty" />
                  <span class="price"><span>x</span> $25.00</span>
                </div>
                <div class="item-block ib-total-price">
                  <span class="tp-price">$75.00</span>
                  <span class="tp-remove"><i class="i-cancel-circle"></i></span>
                </div>         
              </div>
              <div class="bundle-block">
                <ul>
                  <li class="cf">
                    <i class="i-down-right-arrow"></i>
                    <img class="bundle-img" src="http://fakeimg.pl/100/e5e5e5/adadad/?text=IMG" />
                    <span class="bundle-title">Nalgene Orange Cap</span>
                    <span class="bundle-itemno">#9872656</span>
                  </li>
                <li class="cf">
                    <i class="i-down-right-arrow"></i>
                    <img class="bundle-img" src="http://fakeimg.pl/100/e5e5e5/adadad/?text=IMG" />
                    <span class="bundle-title">Nalgene Orange Cap</span>
                    <span class="bundle-itemno">#9872656</span>
                  </li>            
                </ul>
      
              </div>
              <div class="item-foot cf">
      
                <div class="if-left"><span class="if-status">In Stock</span></div>
                <div class="if-right"><span class="blue-link">Gift Options</span> | <span class="blue-link">Add to Registry</span> | <span class="blue-link">Add to Wishlist</span></div>
              </div>
            </li> */}
      {/* <!-- end bundle product --> */}
          </ul>
        </div>
        <div class="sub-table cf">
          <div class="summary-block">
            {/* <div class="sb-promo">
              <input type="text" placeholder="Enter Promo Code" />
              <span class="btn">Apply</span>
            </div>         */}
            <ul>
              <li class="subtotal"><span class="sb-label">Subtotal</span><span class="sb-value">${props.cart_total}</span></li>
              <li class="shipping"><span class="sb-label">Shipping</span><span class="sb-value">${shipping}</span></li>
              {/* <li class="tax"><span class="sb-label">Est. Tax | <span class="tax-edit">edit <i class="i-notch-down"></i></span></span><span class="sb-value">$5.00</span></li> */}
              <li class="tax-calculate"><input type="text" value="06484" class="tax" /><span class="btn">Calculate</span></li>
              <li class="grand-total"><span class="sb-label">Total</span><span class="sb-value">${props.cart_total+shipping}</span></li>
            </ul>
          </div>
          {/* <div class="copy-block">    
            <p>Items will be saved in your cart for 30 days. To save items longer, add them to your <a href="#">Wish List</a>.</p>
            <p class="customer-care">
              Call us M-F 8:00 am to 6:00 pm EST<br />
              (877)-555-5555 or <a href="#">contact us</a>. <br />     
            </p>
          </div>     */}
        </div>
        
        <div class="cart-footer cf">
            {/* <span class="btn">Checkout</span> */}
          <div style={{display:"flex" , justifyContent:"center" , alignItems:"center"}}>
          <Checkout total={props.cart_total}/>
          </div>
            <span class="cont-shopping"><i class="i-angle-left"></i>Continue Shopping</span>    
        </div>
      </div>
    )
}
const mapStateToProps = (state) => {
  return {
    cart: state.cartReducer.cart,
    cart_total:state.cartReducer.cart_total,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    update_total: (total) => dispatch({type:"UPDATE_TOTAL", payload: {total: total}}),
    remove_from_cart: (productId) =>
    dispatch({ type: "REMOVE_FROM_CART", payload: { productId: productId } }),
    add_cart_length: (cart_length) =>
    dispatch({ type: "ADD_CART_LENGTH", payload: { cart_length: cart_length } }),
  };
};
//export default connect(mapStateToProps, mapDispatchToProps)(Cart);
export default  Cart;