import React, { Fragment, useEffect, useState } from 'react';
import { UserMenu } from '../../elements/user_menu';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { functions } from '../../functions';


export const Watch_list = () => {

  const [auctions, setAuctions] = useState([]);
  const Data = useSelector((data) => data);

  console.log(Data);

  useEffect(() => {
    PageData();
    functions.hide_dropdown(1);

  }, []);

  const DeleteAuction = async (aid, index) => {
    // let req = await axios.post(`http://dev.propennyauction.com/api/watchlist_add-del.php?action=remove-watchlist&user_id=${window.localStorage.getItem("user_id")}&auction_id=${aid}`)
    // let res = await req;
    // console.log(auctions);
    let duplicateAuctions = [...auctions];
    duplicateAuctions.splice(index, 1);
    setAuctions(duplicateAuctions);
    functions.flash_msg('<div class="center_popup">Records Deleted Successfully</div>');

  }

  const PageData = async () => {
    let req = await axios.get(" http://dev.propennyauction.com/api/watchlist.php?user_id=" + window.localStorage.getItem("user_id"));
    let res = await req;
    if (res.data.success) {
      setAuctions(res.data.data);
    }
  }
  console.log("rendered");
  return (
    <Fragment>

      <UserMenu />
      <div className="doc_width shadow_bg">
        <div className="step_titel mar_b_0">
          <h1>Watch List</h1>
        </div>
        <div className="table-responsive">
          <div id="rightcol">
            <table className="results" width="100%" cellSpacing="0" cellPadding="0" border="0">
              <tbody><tr>
                <th> Image</th>
                <th> id</th>
                <th> auction id</th>
                <th> Title</th>
                <th> End Time </th>
                <th> Price </th>
                <th> Actions </th>
              </tr>
                {

                  auctions.map((auction, i) => {
                    return (
                      <React.Fragment key={i}>
                        <tr>
                          <td className="normal_bg padd" colSpan=""><img src={`${auction.bids}`} /></td>
                          <td className="normal_bg padd" colSpan="">{auction.id} </td>
                          <td className="normal_bg padd" colSpan="">{auction.auction_id} </td>
                          <td className="normal_bg padd" colSpan="">{auction.title} </td>
                          <td className="normal_bg padd" colSpan="">{auction.end_time} </td>
                          <td className="normal_bg padd" colSpan="">{auction.price} </td>
                          <td className="normal_bg padd buttons buttons-del" colSpan="">
                            <button onClick={() => DeleteAuction(auction.auction_id, i)}>DELETE</button>
                          </td>
                        </tr>


                      </React.Fragment>
                    )
                  })
                }






              </tbody></table>
            <table width="100%" cellSpacing="10" cellPadding="0" border="0">
              <tbody><tr><td align="center"></td>
              </tr></tbody></table>

          </div>
        </div>



      </div>
    </Fragment>
  )
}